//
//  Constants.swift
//  Quicket
//
//  Created by Matthew Stephens on 11/7/14.
//  Copyright (c) 2014 TeamQuicket. All rights reserved.
//

import Foundation

let CurrentLocationDidChangeNotification = "CurrentLocationDidChangeNotification"
