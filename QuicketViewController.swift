//
//  QuicketViewController.swift
//  Quicket
//
//  Created by Matthew Stephens on 10/8/14.
//  Copyright (c) 2014 TeamQuicket. All rights reserved.
//

import Foundation


class QuicketViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var quicketSegmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var rows = 0
    var enteredText = UITextField()
    var results : NSArray = NSArray()
    var currentLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NSNotificationCenter.defaultCenter().addObserver(self.tableView, selector: "locationDidChange", name: CurrentLocationDidChangeNotification, object: nil)

        
    }
    
    
    @IBAction func indexChanged(sender: UISegmentedControl)
    {
        switch quicketSegmentedControl.selectedSegmentIndex
            {
        case 0:
            println("NEW"); //sorts by created at
            var descriptor: NSSortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
            var sortedResults: NSArray = self.results.sortedArrayUsingDescriptors([descriptor])
            
            self.results = sortedResults
            self.tableView.reloadData()
        case 1:
            println("HOT"); //sorts by text since no score atm
            var descriptor: NSSortDescriptor = NSSortDescriptor(key: "text", ascending: false)
            var sortedResults: NSArray = self.results.sortedArrayUsingDescriptors([descriptor])
            
            self.results = sortedResults
            self.tableView.reloadData()
        case 2:
            println("Just me"); //sorts by ID since not marked as just me atm
            var descriptor: NSSortDescriptor = NSSortDescriptor(key: "objectId", ascending: false)
            var sortedResults: NSArray = self.results.sortedArrayUsingDescriptors([descriptor])
            
            self.results = sortedResults
            self.tableView.reloadData()
        default:
            break; 
        } 
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        var query = PFQuery(className:"Post")
        query.findObjectsInBackgroundWithBlock({(NSArray objects, NSError error) in
            if (error != nil) {
            }
            else {
                self.results = NSArray(array: objects)
                self.tableView.reloadData()
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let quicketCellIdentifier = "QuicketCell"
        var cell=self.tableView.dequeueReusableCellWithIdentifier(quicketCellIdentifier, forIndexPath: indexPath) as? UITableViewCell
        
        var post: AnyObject = self.results[indexPath.row]
        var textLabel = cell?.viewWithTag(1) as UILabel
        var dateLabel = cell?.viewWithTag(2) as UILabel
        println(post)
        textLabel.text = post["text"] as NSString
        var formatter = NSDateFormatter()
        formatter.dateFormat = "EEE, MMM d, h:mm a"
        dateLabel.text = formatter.stringFromDate(post.createdAt)
        
        return cell!
    }
    
    func setCurrentLocation(currentLocation : CLLocation) {
        if (self.currentLocation == currentLocation) {
            return;
        }
        
        self.currentLocation = currentLocation
        dispatch_async(dispatch_get_main_queue(), {
            NSNotificationCenter.defaultCenter().postNotificationName(CurrentLocationDidChangeNotification, object: nil)
            })
    }
    
    func locationDidChange(note : NSNotification) {
        // update table with points
    }
    
    func locationManager(locationManager : CLLocationManager, newLocation : CLLocation, oldLocation : CLLocation) {
        self.currentLocation = newLocation
    }
    
}
