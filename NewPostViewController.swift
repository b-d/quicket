//
//  NewPostViewController.swift
//  Quicket
//
//  Created by Matthew Stephens on 10/28/14.
//  Copyright (c) 2014 TeamQuicket. All rights reserved.
//

import Foundation

class NewPostViewController : UIViewController, UITextViewDelegate {
    
    var postText : NSString!

    @IBOutlet weak var postTextView: UITextView!
    
    @IBAction func doneButton(sender : UIBarButtonItem) {
        
        self.postTextView.resignFirstResponder()
        
        var currentUser = PFUser.currentUser()
//        var currentCoordinate = currentLocation.coordinate as CLLocationCoordinate2D;
        
        var post = PFObject(className:"Post")
        post["text"] = self.postText
        post.saveInBackgroundWithBlock { (success : Bool, error : NSError!) -> Void in
            if (success) {
                self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
            }
            else {
                let alert = UIAlertView(title: "Failed", message: "Post failed to send!", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        }

    }
    
    @IBAction func cancelButton(sender : UIBarButtonItem) {
        navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        self.postText = textView.text
        println(self.postText)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        self.postTextView.becomeFirstResponder()
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}